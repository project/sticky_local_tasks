<?php

declare(strict_types=1);

namespace Drupal\sticky_local_tasks;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Builds render arrays for sticky local tasks.
 */
class StickyLocalTasksBuilder {

  /**
   * Creates the sticky task manager.
   *
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   *   The admin context service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Menu\LocalTaskManagerInterface $localTaskManager
   *   The local task manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   */
  public function __construct(
    protected readonly AdminContext $adminContext,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly LocalTaskManagerInterface $localTaskManager,
    protected readonly RouteMatchInterface $routeMatch,
  ) {}

  /**
   * Returns a render array for sticky local tasks.
   *
   * @param \Drupal\sticky_local_tasks\Position $position
   *   The position to display the links in.
   *
   * @return array
   *   The sticky local tasks render array.
   */
  public function build(Position $position): array {
    $build = [];
    $links = $this->localTaskManager->getLocalTasks($this->routeMatch->getRouteName());
    $cacheability = $links['cacheability'];
    assert($cacheability instanceof CacheableMetadata);
    // Do not display single tabs.
    $tabs = $links['tabs'];
    if (count(Element::getVisibleChildren($tabs)) < 2) {
      $cacheability->applyTo($build);
      return $build;
    }

    foreach ($tabs as $route_name => $element) {
      $this->addSuggestion('menu_local_task__sticky_local_tasks', $tabs[$route_name]);
      $class = $this->getCssClass($route_name);
      if ($class) {
        $tabs[$route_name]['#attributes']['class'][] = $class;
      }
    }

    $tabs['#position'] = $position->value;

    $build += [
      '#theme' => 'menu_local_tasks__sticky_local_tasks',
      '#primary' => $tabs,
      '#attached' => [
        'library' => ['sticky_local_tasks/sticky-local-tasks'],
      ],
    ];

    $cacheability->addCacheableDependency($this->localTaskManager)
      ->applyTo($build);
    return $build;
  }

  /**
   * Adds sticky tasks to the build array per config settings.
   *
   * @param array $build
   *   The build array to alter.
   */
  public function addToPage(array &$build): void {
    $config = $this->getConfig();
    $usage = $config->get('usage');
    $build['sticky_local_tasks']['#cache']['tags'][] = 'config:sticky_local_tasks.settings';
    if ($usage !== 'all') {
      return;
    }

    // @todo Make this a path-based config property like block display settings.
    $build['sticky_local_tasks']['#cache']['contexts'][] = 'route';
    if (in_array($this->routeMatch->getRouteName(), $this->getRestrictedRoutes(), TRUE)) {
      return;
    }

    $options = $config->get('usage_options');
    if (!$options['show_on_admin'] && $this->adminContext->isAdminRoute()) {
      return;
    }

    $cacheability = CacheableMetadata::createFromRenderArray($build['sticky_local_tasks']);
    $position = Position::from($options['static_position']);
    $build['sticky_local_tasks'] = $this->build($position);
    $cacheability->applyTo($build['sticky_local_tasks']);
  }

  /**
   * Adds a theme suggestion to an element.
   *
   * If #theme is a string, it will be overridden with the suggestion; if it's
   * an array, the suggestion will be prepended to it.
   *
   * @param string $suggestion
   *   The full theme hook with suggestion, eg node__teaser.
   * @param array $element
   *   The element to update.
   */
  protected function addSuggestion(string $suggestion, array &$element): void {
    if (is_array($element['#theme'])) {
      array_unshift($element['#theme'], $suggestion);
    }
    else {
      assert(is_string($element['#theme']));
      $element['#theme'] = $suggestion;
    }
  }

  /**
   * Returns an array of route names that shouldn't have sticky tasks.
   *
   * It's not used for the block (it already has customizable path visibility).
   *
   * @return string[]
   *   An array of route names.
   */
  protected function getRestrictedRoutes(): array {
    return [
      'user.login',
      'user.register',
      'user.pass',
    ];
  }

  /**
   * Returns a CSS class to add to a sticky local task based on its route.
   *
   * @param string $route_name
   *   The task route name.
   *
   * @return string|null
   *   The CSS class to add if one exists; NULL otherwise.
   */
  protected function getCssClass(string $route_name): ?string {
    // @todo This might be neater detecting entity.*.* routes.
    // @todo Provide event for others to alter.
    $route_to_link_name_map = [
      'canonical' => 'view',
      'edit_form' => 'edit',
      'delete_form' => 'delete',
      'version_history' => 'revisions',
      'devel' => 'devel',
      'content_translation_overview' => 'translate',
      'entity_clone' => 'clone',
      'quick_node_clone' => 'clone',
      'entityqueue' => 'entityqueue',
      'zip' => 'zip',
      'export' => 'export',
      'pdf' => 'pdf',
      'analytics' => 'analytics',
      'xml' => 'xml',
      'view_issue' => 'magazine',
      'import' => 'import',
      'permission' => 'permission',
      'grantform' => 'grants',
      'layout_builder' => 'layout',
      'webform.edit_form' => 'edit',
      'webform.results_submissions' => 'results',
      'webform.test_form' => 'test',
      'webform.references' => 'references',
      'webform.export_form' => 'export',
      'webform.settings' => 'settings',
      'webform_submission.user' => 'submission',
      'node.display' => 'manage-display',
      'user.display' => 'manage-display',
      'node.convert_bundles' => 'convert',
      'shortcut.set_switch' => 'shortcuts',
    ];
    foreach ($route_to_link_name_map as $route_part => $name) {
      if (strpos($route_name, $route_part) !== FALSE) {
        return 'nav-item--' . $name;
      }
    }
    return NULL;
  }

  /**
   * Gets the module's config settings.
   */
  protected function getConfig(): ImmutableConfig {
    return $this->configFactory->get('sticky_local_tasks.settings');
  }

}
