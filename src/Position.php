<?php

declare(strict_types=1);

namespace Drupal\sticky_local_tasks;

/**
 * Enumerates the positions sticky local tasks can be shown.
 */
enum Position: string {
  case BottomLeft = 'bottom-left';
  case BottomRight = 'bottom-right';

  /**
   * Returns the possible backed values for validation.
   */
  public static function values(): array {
    return array_map(fn (Position $position): string => $position->value, self::cases());
  }

}
