<?php

declare(strict_types=1);

namespace Drupal\sticky_local_tasks\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\Url;
use Drupal\sticky_local_tasks\Position;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures forms module settings.
 */
class StickyLocalTasksForm extends ConfigFormBase {

  /**
   * The element info manager.
   */
  protected ElementInfoManagerInterface $elementInfo;

  /**
   * The module handler service.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $form = parent::create($container);
    $form->elementInfo = $container->get('plugin.manager.element_info');
    $form->moduleHandler = $container->get('module_handler');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sticky_local_tasks_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'sticky_local_tasks.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $config = $this->getConfig();
    if ($this->moduleHandler->moduleExists('block')) {
      $context = [
        ':block_layout' => Url::fromRoute('block.admin_display')->toString(),
      ];
      $block_label = $this->t('Use <i>Sticky primary tabs</i> block or custom code.');
      $block_description = $this->t('Select this is if you\'re going to <a href=":block_layout">place the <i>Sticky primary tabs</i> block</a>, or if you\'re going to directly add the sticky tabs via code, eg. in a custom template. Sticky local tasks will only show up where you configure them to. For more details see the project README.md.', $context);
    }
    else {
      $block_label = $this->t('Use custom code.');
      $block_description = $this->t("Select this is if you're going to directly add the sticky tabs via code, eg. in a custom template. Sticky local tasks will only show up where they're explicitly added. For more details see the project README.md.");
    }
    $form['usage'] = [
      '#type' => 'radios',
      '#title' => $this->t('Usage'),
      '#options' => [
        'block' => $block_label,
        'all' => $this->t('Add sticky primary tabs to all pages on all themes.'),
      ],
      '#required' => TRUE,
      '#default_value' => $config->get('usage'),
      '#ajax' => [
        'callback' => '::usageOptionsCallback',
        'wrapper' => 'sticky-local-tasks-usage-options',
      ],
      'block' => [
        '#description' => $block_description,
      ],
    ];

    $usage = $form_state->getValue('usage') ?: $config->get('usage');
    $usage_options_form = match($usage) {
      'all' => $this->allPagesOptionsForm($config->get('usage_options')),
      default => [],
    };

    if ($usage_options_form) {
      $form['usage_options'] = [
        // Add the default theme wrappers so that the 'container' added later
        // won't override the theming.
        '#theme_wrappers' => $this->elementInfo->getInfoProperty('details', '#theme_wrappers'),
        '#type' => 'details',
        '#title' => $this->t('Options'),
        '#open' => TRUE,
        '#tree' => TRUE,
      ] + $usage_options_form;
    }

    $form['usage_options']['#theme_wrappers']['container']['#attributes']['id'] = 'sticky-local-tasks-usage-options';

    return $form;
  }

  /**
   * Returns the usage options form for adding sticky tasks to all pages/themes.
   *
   * @param array $default_values
   *   An associative array of default values keyed by name.
   *
   * @return array
   *   The usage options form elements to be added to the details element.
   */
  public function allPagesOptionsForm(array $default_values): array {
    $form['show_on_admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show on admin pages.'),
      '#default_value' => $default_values['show_on_admin'],
    ];

    $form['static_position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select your preferred sticky local tasks position'),
      '#options' => [
        Position::BottomRight->value => $this->t('Bottom Right'),
        Position::BottomLeft->value => $this->t('Bottom Left'),
      ],
      '#required' => TRUE,
      '#default_value' => $default_values['static_position'] ?? 'bottom-right',
    ];
    return $form;
  }

  /**
   * Returns the usage options section of the form array.
   */
  public static function usageOptionsCallback(array &$form, FormStateInterface $form_state): array {
    return $form['usage_options'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();
    $form_state->cleanValues();
    $config->setData($form_state->getValues() + [
      'usage_options' => [],
    ]);

    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Gets the module's editable configuration.
   *
   * @return \Drupal\Core\Config\Config
   *   The configuration.
   */
  public function getConfig(): Config {
    return $this->config('sticky_local_tasks.settings');
  }

}
