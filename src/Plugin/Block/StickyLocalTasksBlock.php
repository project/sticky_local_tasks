<?php

declare(strict_types=1);

namespace Drupal\sticky_local_tasks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sticky_local_tasks\Position;
use Drupal\sticky_local_tasks\StickyLocalTasksBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a sticky local tasks block.
 *
 * @Block(
 *   id = "sticky_local_tasks",
 *   admin_label = @Translation("Sticky primary tabs"),
 * )
 */
class StickyLocalTasksBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Creates the sticky local tasks block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\sticky_local_tasks\StickyLocalTasksBuilder $stickyLocalTasksBuilder
   *   The sticky local tasks builder.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    mixed $plugin_definition,
    protected readonly StickyLocalTasksBuilder $stickyLocalTasksBuilder,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $block = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('sticky_local_tasks.builder')
    );
    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return ['position' => Position::BottomRight->value];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select your preferred sticky local tasks position'),
      '#options' => [
        Position::BottomRight->value => $this->t('Bottom right'),
        Position::BottomLeft->value => $this->t('Bottom left'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->configuration['position'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['position'] = $form_state->getValue('position');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $position = Position::from($this->configuration['position']);
    return $this->stickyLocalTasksBuilder->build($position);
  }

}
