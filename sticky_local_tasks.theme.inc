<?php

/**
 * @file
 * Provides theme hooks and preprocess functions for sticky local tasks.
 */

declare(strict_types=1);

use Drupal\Core\Cache\CacheableMetadata;

/**
 * Implements hook_preprocess_menu_local_tasks__sticky_local_tasks().
 */
function sticky_local_tasks_preprocess_menu_local_tasks__sticky_local_tasks(&$variables): void {
  $variables['position'] = $variables['primary']['#position'];
}

/**
 * Implements hook_preprocess_menu_local_task__sticky_local_tasks().
 */
function sticky_local_tasks_preprocess_menu_local_task__sticky_local_tasks(&$variables): void {
  $link = $variables['link'];
  $variables['url'] = $link['#url'];
  $variables['text'] = $link['#title'];
  // By default there's no #cache on the link element, but it's possible the
  // link's been altered and cacheability added.
  $cacheability = CacheableMetadata::createFromRenderArray($link);
  $cacheability->applyTo($variables);
}
