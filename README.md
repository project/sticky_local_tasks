## Sticky Local Tasks

There are times when you are on a long page and you have to scroll up to the top
of the page to access the local tasks (also known as primary tabs) such as
_View_, _Edit_, etc. This module provides a sticky icon on the bottom right of
the page that holds all the local task items for easier access. Icons are from
icons8 and under glyph-neue category.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules].

[Installing Drupal Modules]:https://www.drupal.org/docs/extending-drupal/installing-drupal-modules

## Configuration

There are three ways you can add sticky local tasks to pages:
1. Configure the module to add them to pages regardless of theme, optionally
   filtering out admin routes.
   1. Go to `admin/config/user-interface/sticky-local-tasks`.
   2. Select _Add sticky primary tabs to all pages on all themes_.
   3. If you want them to show up on admin routes, check _Show on admin pages_.
   4. Set the desired position.
   5. Save the configuration.
2. Place and configure the _Sticky primary tabs_ block.
   1. Go to `admin/config/user-interface/sticky-local-tasks`.
   2. Select _Use Sticky primary tabs block or custom code_.
   3. Save the configuration.
   4. Go to `admin/structure/block`.
   5. Optionally choose the theme you want the sticky tasks to show up in.
   6. Choose a region and click _Place block_. The region only affects the
      position of the tabs in the markup, not where they visually appear.
   7. Configure the block: select the visual position for the sticky tabs and
      any other configuration you'd like, eg. show the block only on certain
      pages or for certain roles.
   8. Save the configuration.
3. Add the links directly in a template or preprocess.
   1. Go to `admin/config/user-interface/sticky-local-tasks`.
   2. Select the usage option for _custom code_.
   3. Save the configuration.
   4. The following code returns the render array:
      ```php
      $position = \Drupal\sticky_local_tasks\Position::BottomLeft;
      $build = \Drupal::service('sticky_local_tasks.builder')->build($position);
      ```

## Maintainers

Sohail Lajevardi: https://www.drupal.org/u/doxigo
