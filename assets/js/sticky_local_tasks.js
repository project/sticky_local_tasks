(function (Drupal, once) {
  'use strict';

  Drupal.behaviors.stickyLocalTasks = {
    attach(context) {
      const [toggle] = once(
        'sticky-local-tasks',
        '[data-sticky-local-tasks-toggle]',
        context,
      );
      if (!toggle) {
        return;
      }

      const items = context.querySelector('[data-sticky-local-tasks-items]');

      // Show the sticky local tasks items
      function showStickyLocalTasksItems() {
        items.style.display = 'flex';
        items.classList.add('show');
      }

      // Hide the sticky local tasks items
      function hideStickyLocalTasksItems() {
        // @todo If it's just the case that we want display none when the
        //   animation/transition finishes, can we not do that entirely with
        //   CSS?
        setTimeout(function () {
          items.style.display = 'none';
        }, 200);
        items.classList.remove('show');
      }

      toggle.addEventListener('click', function () {
        const show = !items.classList.contains('show');
        if (show) {
          showStickyLocalTasksItems();
        } else {
          hideStickyLocalTasksItems();
        }
        toggle.classList.toggle('active', show);
        toggle.setAttribute('aria-expanded', show ? 'true' : 'false');
      });
    },
  };
})(Drupal, once);
