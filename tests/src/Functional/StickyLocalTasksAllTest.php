<?php

declare(strict_types=1);

namespace Drupal\Tests\sticky_local_tasks\Functional;

use Drupal\Core\Url;
use Drupal\sticky_local_tasks\Position;

/**
 * Tests sticky local tasks when configured to be added to all pages.
 *
 * @group sticky_local_tasks
 */
class StickyLocalTasksAllTest extends StickyLocalTasksTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $user = $this->drupalCreateUser([
      'administer site configuration',
      'bypass node access',
      'administer nodes',
      'administer users',
      'administer permissions',
    ]);
    $this->drupalLogin($user);
  }

  /**
   * Tests that nothing is added when not configured to be.
   */
  public function testNotAdded(): void {
    $this->drupalGet('admin/config/user-interface/sticky-local-tasks');
    $this->getSession()->getPage()->selectFieldOption('usage', 'block');
    $this->getSession()->getPage()->pressButton('edit-submit');

    $this->drupalGet($this->node->toUrl());

    $this->assertSession()->linkByHrefNotExistsExact($this->node->toUrl()->toString());
    $this->assertSession()->linkByHrefNotExistsExact($this->node->toUrl('edit-form')->toString());
    $this->assertSession()->linkByHrefNotExistsExact($this->node->toUrl('delete-form')->toString());
    $this->assertSession()->linkByHrefNotExistsExact($this->node->toUrl('revision')->toString());

    $this->drupalGet('admin/people');

    $this->assertSession()->linkByHrefNotExistsExact('/admin/people');
    $this->assertSession()->linkByHrefNotExistsExact('/admin/people/permissions');
  }

  /**
   * Tests sticky tasks being added to every page.
   *
   * @param \Drupal\sticky_local_tasks\Position $position
   *   The position to add the sticky tasks to.
   *
   * @dataProvider positions
   */
  public function testAll(Position $position): void {
    $this->drupalGet('admin/config/user-interface/sticky-local-tasks');
    $this->getSession()->getPage()->selectFieldOption('usage', 'all');
    $this->getSession()->getPage()->uncheckField('usage_options[show_on_admin]');
    $this->getSession()->getPage()->selectFieldOption('usage_options[static_position]', $position->value);
    $this->getSession()->getPage()->pressButton('edit-submit');

    $this->drupalGet($this->node->toUrl());

    $this->assertSession()->linkByHrefExistsExact($this->node->toUrl()->toString());
    $this->assertSession()->linkByHrefExistsExact($this->node->toUrl('edit-form')->toString());
    $this->assertSession()->linkByHrefExistsExact($this->node->toUrl('delete-form')->toString());
    $this->assertSession()->linkByHrefExistsExact($this->node->toUrl('revision')->toString());
    // As far as I know the only way to actually get the position of the element
    // on screen would be with a FunctionalJavascript test, using JS. For now
    // let's have quicker tests by just checking the class is added.
    $this->assertSession()->elementAttributeContains('css', '.sticky-local-tasks__wrapper', 'class', 'sticky-local-tasks--' . $position->value);

    $this->drupalGet('admin/people');
    $this->assertSession()->linkByHrefNotExistsExact(Url::fromUserInput('/admin/people')->toString());
    $this->assertSession()->linkByHrefNotExistsExact(Url::fromUserInput('/admin/people/permissions')->toString());

    $this->drupalGet('admin/config/user-interface/sticky-local-tasks');
    $this->getSession()->getPage()->checkField('usage_options[show_on_admin]');
    $this->getSession()->getPage()->pressButton('edit-submit');

    $this->drupalGet('admin/people');
    $this->assertSession()->linkByHrefExistsExact(Url::fromUserInput('/admin/people')->toString());
    $this->assertSession()->linkByHrefExistsExact(Url::fromUserInput('/admin/people/permissions')->toString());
    $this->assertSession()->elementAttributeContains('css', '.sticky-local-tasks__wrapper', 'class', 'sticky-local-tasks--' . $position->value);
  }

}
