<?php

declare(strict_types=1);

namespace Drupal\Tests\sticky_local_tasks\Functional;

use Drupal\sticky_local_tasks\Position;

/**
 * Tests sticky local tasks when add via the block.
 *
 * @group sticky_local_tasks
 */
class StickyLocalTasksBlockTest extends StickyLocalTasksTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $user = $this->drupalCreateUser([
      'administer site configuration',
      'bypass node access',
      'administer nodes',
      'administer blocks',
    ]);
    $this->drupalLogin($user);
  }

  /**
   * Tests sticky tasks being added via a block.
   *
   * @param \Drupal\sticky_local_tasks\Position $position
   *   The position to add the sticky tasks to.
   *
   * @dataProvider positions
   */
  public function testBlock(Position $position): void {
    $this->drupalGet('admin/config/user-interface/sticky-local-tasks');
    $this->getSession()->getPage()->selectFieldOption('usage', 'block');
    $this->getSession()->getPage()->pressButton('edit-submit');

    $this->drupalGet('admin/structure/block');
    $this->getSession()->getPage()->findLink('Place block')->click();
    $this->assertSession()->linkByHrefExists('/admin/structure/block/add/sticky_local_tasks/stark');
    $this->drupalPlaceBlock('sticky_local_tasks', ['position' => $position->value]);

    $this->drupalGet($this->node->toUrl());

    $this->assertSession()->linkByHrefExistsExact($this->node->toUrl()->toString());
    $this->assertSession()->linkByHrefExistsExact($this->node->toUrl('edit-form')->toString());
    $this->assertSession()->linkByHrefExistsExact($this->node->toUrl('delete-form')->toString());
    $this->assertSession()->linkByHrefExistsExact($this->node->toUrl('revision')->toString());
    $this->assertSession()->elementAttributeContains('css', '.sticky-local-tasks__wrapper', 'class', 'sticky-local-tasks--' . $position->value);
  }

}
