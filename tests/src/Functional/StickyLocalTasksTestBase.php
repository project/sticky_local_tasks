<?php

declare(strict_types=1);

namespace Drupal\Tests\sticky_local_tasks\Functional;

use Drupal\node\NodeInterface;
use Drupal\sticky_local_tasks\Position;
use Drupal\Tests\BrowserTestBase;

/**
 * Provides a base test class for sticky local tasks functional tests.
 */
abstract class StickyLocalTasksTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sticky_local_tasks', 'node'];

  /**
   * A node created to test sticky local tasks.
   */
  protected NodeInterface $node;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'page']);
    $this->node = $this->drupalCreateNode([
      'title' => 'Test node',
      'type' => 'page',
    ]);
  }

  /**
   * Returns data for tests expecting all possible position values.
   */
  public function positions(): iterable {
    yield [Position::BottomRight];
    yield [Position::BottomLeft];
  }

}
